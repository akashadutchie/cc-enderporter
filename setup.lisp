(import cc/term term)
(import cc/settings sett)
(import cc/keys keys)
(import cc/fs fs)
(import cc/os os)
(import cc/peripheral peri)
(import data/string (repeater))
;;(defun repeater (token nchars)
;;  "Repeats TOKEN to match NCHARS characters.
;;   Wraps and crops TOKEN to do so.
;;   ```
;;   (str/repeater (\"hello world\" 5))
;;   out = \"hello\"
;;   ```"
;;  (let* (
;;      (tn (n token)) 
;;      (rep* (/ nchars tn)) 
;;      (rem* (- 0 (- tn (mod (- nchars 1) tn)))))
;;    (string/sub (string/rep token (math/ceil rep*)) 1 (if (= 0 rem*) nil rem*))))

(define __rainbow-pt "e1453a2")
(define __black-pt "f")
(define __gray-pt "7")
(define __light-gray-pt "8")
(define __white-pt "0")

(defun blit (text foreground-pt background-pt)
  (term/blit text 
    (if (exists? foreground-pt) (repeater foreground-pt (n text)) __white-pt) 
    (if (exists? background-pt) (repeater background-pt (n text)) __black-pt))
  (print! ""))

(defun manual-find-drive ()
  (print! "Cant find myself. Where am I?")
  (print! "Looking for 'enderporter_setup':")
  (while (with (usr-in (io/read)) (cond
    ((fs/isDir usr-in) (progn (os/run "ls" usr-in) true))
    ((fs/exists usr-in) (progn usr-in false))
    (else (progn "not there" true))))))

(define program-fp 
  (if-with (drives (map (λ(wr) ((.> wr "getMountPath"))) (filter (λ(dr) (= "enderporter_setup" ((.> dr "getDiskLabel")))) (list (peri/find "drive")))))
    (if (empty? drives)
      (manual-find-drive)
      (with (candids (filter fs/exists (map (λ(drfp)(.. drfp "/pearl_control.lua")) drives)))
        (if (empty? candids)
          (error! "Pearl control program removed from disk")
          (car candids))))
    (manual-find-drive)))

(term/clear)
(term/setCursorPos 1 1)
(print! program-fp)
(blit "--= Enderporter Setup =--" __black-pt __rainbow-pt)

(if (or (nil? (os/getComputerLabel)) (not (string/starts-with? (os/getComputerLabel) "enderporter:")))
  (progn
    (print! "Set the teleporter label:")
    (while (with (usr-in (io/read)) (cond
      ;; TODO resolve conflicts with those already existing on network
      ;;((?) true)
      ((empty? usr-in) true)
      (else (progn (os/setComputerLabel (.. "enderporter:" usr-in)) false))))))
  else)

(blit "Specify side to output to:" __black-pt __light-gray-pt)
(blit "Signal must be inverted (e.g rs torch)" __gray-pt __black-pt)
(print! "- Up")
(print! "- Down")
(print! "- Front")
(print! "- Back")
(print! "- Left")
(print! "- Right")
(while (with (k (second (os/pullEvent "key"))) (cond 
  ((= k keys/u) (progn (sett/set "emit-side" "top") false))
  ((= k keys/d) (progn (sett/set "emit-side" "bottom") false))
  ((= k keys/f) (progn (sett/set "emit-side" "front") false))
  ((= k keys/b) (progn (sett/set "emit-side" "back") false))
  ((= k keys/l) (progn (sett/set "emit-side" "left") false))
  ((= k keys/r) (progn (sett/set "emit-side" "right") false))
  (else true))))

(print! (sett/get "emit-side"))
(print! "Copying program over...")
;;(os/run {} "copy" program-fp "startup.lua")
(if (fs/exists "/startup.lua") (fs/delete "/startup.lua") else)
(fs/copy program-fp "/startup.lua")
(print! "Saving setup...")
(sett/save "enderporter.settings")

(print! "")
(blit "Broacast rednet on teleport? [Y/n]" __black-pt __light-gray-pt)
(while (with (k (second (os/pullEvent "key"))) (cond 
  ((= k keys/y) (progn (sett/set "broadcast-attempt" "true") false))
  ((= k keys/n) (progn (sett/set "broadcast-attempt" "false") false))
  ((= k keys/enter) (progn (sett/set "broadcast-attempt" "true") false))
  (else true))))
(print! (sett/get "broadcast-attempt"))

(print! "Saving setup...")
(sett/save "enderporter.settings")