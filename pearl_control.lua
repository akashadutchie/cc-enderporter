if not table.pack then table.pack = function(...) return { n = select("#", ...), ... } end end
if not table.unpack then table.unpack = unpack end
local load = load if _VERSION:find("5.1") then load = function(x, n, _, env) local f, e = loadstring(x, n) if not f then return f, e end if env then setfenv(f, env) end return f end end
local _select, _unpack, _pack, _error = select, table.unpack, table.pack, error
local _libs = {}
local _3d_1, _2b_1, _2e2e_1, len_23_1, getIdx1, print1, type_23_1, sub1, n1, succ1, setOutput1, open1, broadcast1, receive1, sleep1, getComputerLabel1, queueEvent1, drop1, find1, setCursorPos1, clear1, get1, load1, side1, emitBroadcast_3f_1, token1, broad1, teleports1
_3d_1 = function(v1, v2) return v1 == v2 end
_2b_1 = function(x, ...) local t = x + ... for i = 2, _select('#', ...) do t = t + _select(i, ...) end return t end
_2e2e_1 = function(x, ...) local n = _select('#', ...) local t = _select(n, ...) for i = n - 1, 1, -1 do t = _select(i, ...) .. t end return x .. t end
len_23_1 = function(v1) return #v1 end
getIdx1 = function(v1, v2) return v1[v2] end
print1 = print
type_23_1 = type
sub1 = string.sub
n1 = function(x)
  if type_23_1(x) == "table" then
    return x["n"]
  else
    return #x
  end
end
succ1 = function(x)
  return x + 1
end
setOutput1 = rs.setOutput
open1 = rednet.open
broadcast1 = rednet.broadcast
receive1 = rednet.receive
sleep1 = os.sleep
getComputerLabel1 = os.getComputerLabel
queueEvent1 = os.queueEvent
drop1 = turtle.drop
find1 = peripheral.find
setCursorPos1 = term.setCursorPos
clear1 = term.clear
get1 = settings.get
load1 = settings.load
load1("enderporter.settings")
side1 = get1("emit-side", "top")
emitBroadcast_3f_1 = get1("broadcast-attempt", false)
token1 = getComputerLabel1()
broad1 = sub1(token1, 1 + n1("enderporter:"))
teleports1 = 0
find1("modem", open1)
while true do
  clear1()
  setCursorPos1(1, 1)
  print1("This turtle will cause a teleport on packet " .. token1 .. ", protocol " .. "enderporter")
  print1("Teleports done this session: " .. teleports1)
  local result, message, prot = receive1("enderporter")
  if message == token1 then
    setOutput1(side1, true)
    drop1(1)
    sleep1(0.5)
    setOutput1(side1, false)
    teleports1 = succ1(teleports1)
    if emitBroadcast_3f_1 then
      broadcast1(token1, "enderporter-poll")
    end
    queueEvent1("enderporter-teleported")
  elseif message == "GLOBAL_POLL;" then
    broadcast1(broadcast1(broad1, "enderporter-poll"))
  end
end
