(import cc/rednet rn)
(import cc/peripheral peri)
(import cc/io io)
(import cc/os (sleep pullEvent))
(import cc/parallel (waitForAny))

(peri/find "modem" rn/open)
(if (rn/isOpen)
  nil
  (error! "No modem attached"))

(with (usr-in (car *arguments*)) (case usr-in
  (nil? (waitForAny 
    (λ() (sleep 0.5))
    (λ()
      (rn/broadcast "GLOBAL_POLL;" "enderporter")
      (while true
        (with ((_ label) (rn/receive "enderporter-poll"))
          (print! label))))))
  ("-watch"
    (waitForAny 
      (λ() (pullEvent "key"))
      (λ() (with ((_ label) (rn/receive "enderporter-poll"))
        (print! label)))))
  (else
    (rn/broadcast (.. "enderporter:" usr-in) "enderporter"))))