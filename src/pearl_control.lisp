(import cc/rs rs)
(import cc/rednet rn)
(import cc/os os)
(import cc/turtle turtle)
(import cc/peripheral peripheral)
(import cc/term (clear setCursorPos))
(import cc/settings sett)

(sett/load "enderporter.settings")
(define side (sett/get "emit-side" "top"))
(define emit-broadcast? (sett/get "broadcast-attempt" false))
(define protocol "enderporter")
(define token (os/getComputerLabel))
(define broad (string/sub token (+ 1 (n "enderporter:"))))
(define teleports :mutable 0)
(peripheral/find "modem" rn/open)

(while true 
  (clear)
  (setCursorPos 1 1)
  (print! (.. "This turtle will cause a teleport on packet " token ", protocol " protocol))
  (print! (.. "Teleports done this session: " teleports))
  (with ((result message prot) (rn/receive protocol)) 
    (cond 
      ((= message token) (progn
        (rs/setOutput side true)
        (turtle/drop 1)
        (os/sleep 0.5)
        (rs/setOutput side false)
        (inc! teleports)
        (if emit-broadcast? (rn/broadcast token "enderporter-poll") else)
        (os/queueEvent "enderporter-teleported")))
      ((= message "GLOBAL_POLL;") 
        (rn/broadcast (rn/broadcast broad "enderporter-poll")))
      (else))))