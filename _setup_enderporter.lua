if not table.pack then table.pack = function(...) return { n = select("#", ...), ... } end end
if not table.unpack then table.unpack = unpack end
local load = load if _VERSION:find("5.1") then load = function(x, n, _, env) local f, e = loadstring(x, n) if not f then return f, e end if env then setfenv(f, env) end return f end end
local _select, _unpack, _pack, _error = select, table.unpack, table.pack, error
local _libs = {}
local _3d_1, _2f3d_1, _3c3d_1, _3e3d_1, _2b_1, _2d_1, _2a_1, _2f_1, mod1, expt1, _2e2e_1, len_23_1, getIdx1, setIdx_21_1, error1, getmetatable1, next1, print1, setmetatable1, tostring1, type_23_1, format1, rep1, sub1, concat1, unpack1, n1, car1, list1, _enot1, constVal1, splice1, apply1, first1, second1, table_3f_1, list_3f_1, empty_3f_1, number_3f_1, function_3f_1, exists_3f_1, nil_3f_1, type1, map1, pretty1, traceback1, demandFailure_2d3e_string1, _2a_demandFailureMt_2a_1, demandFailure1, abs1, ceil1, min1, modf1, car2, map2, partition1, filter1, nth1, nths1, push_21_1, startsWith_3f_1, read1, formatOutput_21_1, gcd1, numerator1, denominator1, _2d3e_ratComponents1, normalisedRationalComponents1, repeater1, setCursorPos1, blit1, clear1, set1, get1, save1, n2, r1, u1, y1, b1, d1, f1, l1, enter1, copy1, delete1, isDir1, exists1, pullEvent1, setComputerLabel1, getComputerLabel1, run1, find1, blit2, manualFindDrive1, programFp1
_3d_1 = function(v1, v2) return v1 == v2 end
_2f3d_1 = function(v1, v2) return v1 ~= v2 end
_3c3d_1 = function(v1, v2) return v1 <= v2 end
_3e3d_1 = function(v1, v2) return v1 >= v2 end
_2b_1 = function(x, ...) local t = x + ... for i = 2, _select('#', ...) do t = t + _select(i, ...) end return t end
_2d_1 = function(x, ...) local t = x - ... for i = 2, _select('#', ...) do t = t - _select(i, ...) end return t end
_2a_1 = function(x, ...) local t = x * ... for i = 2, _select('#', ...) do t = t * _select(i, ...) end return t end
_2f_1 = function(x, ...) local t = x / ... for i = 2, _select('#', ...) do t = t / _select(i, ...) end return t end
mod1 = function(x, ...) local t = x % ... for i = 2, _select('#', ...) do t = t % _select(i, ...) end return t end
expt1 = function(x, ...) local n = _select('#', ...) local t = _select(n, ...) for i = n - 1, 1, -1 do t = _select(i, ...) ^ t end return x ^ t end
_2e2e_1 = function(x, ...) local n = _select('#', ...) local t = _select(n, ...) for i = n - 1, 1, -1 do t = _select(i, ...) .. t end return x .. t end
len_23_1 = function(v1) return #v1 end
getIdx1 = function(v1, v2) return v1[v2] end
setIdx_21_1 = function(v1, v2, v3) v1[v2] = v3 end
error1 = error
getmetatable1 = getmetatable
next1 = next
print1 = print
setmetatable1 = setmetatable
tostring1 = tostring
type_23_1 = type
format1 = string.format
rep1 = string.rep
sub1 = string.sub
concat1 = table.concat
unpack1 = table.unpack
n1 = function(x)
  if type_23_1(x) == "table" then
    return x["n"]
  else
    return #x
  end
end
car1 = function(xs)
  return xs[1]
end
list1 = function(...)
  local xs = _pack(...) xs.tag = "list"
  return xs
end
_enot1 = function(expr)
  return not expr
end
constVal1 = function(val)
  if type_23_1(val) == "table" then
    local tag = val["tag"]
    if tag == "number" then
      return val["value"]
    elseif tag == "string" then
      return val["value"]
    else
      return val
    end
  else
    return val
  end
end
splice1 = function(xs)
  local parent = xs["parent"]
  if parent then
    return unpack1(parent, xs["offset"] + 1, xs["n"] + xs["offset"])
  else
    return unpack1(xs, 1, xs["n"])
  end
end
apply1 = function(f, ...)
  local _n = _select("#", ...) - 1
  local xss, xs
  if _n > 0 then
    xss = {tag="list", n=_n, _unpack(_pack(...), 1, _n)}
    xs = select(_n + 1, ...)
  else
    xss = {tag="list", n=0}
    xs = ...
  end
  return f(splice1((function()
    local _offset, _result, _temp = 0, {tag="list"}
    _temp = xss
    for _c = 1, _temp.n do _result[0 + _c + _offset] = _temp[_c] end
    _offset = _offset + _temp.n
    _temp = xs
    for _c = 1, _temp.n do _result[0 + _c + _offset] = _temp[_c] end
    _offset = _offset + _temp.n
    _result.n = _offset + 0
    return _result
  end)()
  ))
end
first1 = function(...)
  local rest = _pack(...) rest.tag = "list"
  return rest[1]
end
second1 = function(...)
  local rest = _pack(...) rest.tag = "list"
  return rest[2]
end
table_3f_1 = function(x)
  return type_23_1(x) == "table"
end
list_3f_1 = function(x)
  return type1(x) == "list"
end
empty_3f_1 = function(x)
  local xt = type1(x)
  if xt == "list" then
    return x["n"] == 0
  elseif xt == "string" then
    return #x == 0
  else
    return false
  end
end
number_3f_1 = function(x)
  return type_23_1(x) == "number" or type_23_1(x) == "table" and x["tag"] == "number"
end
function_3f_1 = function(x)
  return type1(x) == "function" or type1(x) == "multimethod"
end
exists_3f_1 = function(x)
  return _enot1(type_23_1(x) == "nil")
end
nil_3f_1 = function(x)
  return type_23_1(x) == "nil"
end
type1 = function(val)
  local ty = type_23_1(val)
  if ty == "table" then
    return val["tag"] or "table"
  else
    return ty
  end
end
map1 = function(f, x)
  local out = {tag="list", n=0}
  local forLimit = n1(x)
  local i = 1
  while i <= forLimit do
    out[i] = f(x[i])
    i = i + 1
  end
  out["n"] = n1(x)
  return out
end
local this = {lookup={list=function(xs)
  return "(" .. concat1(map1(pretty1, xs), " ") .. ")"
end, symbol=function(x)
  return x["contents"]
end, key=function(x)
  return ":" .. x["value"]
end, number=function(x)
  return format1("%g", constVal1(x))
end, string=function(x)
  return format1("%q", constVal1(x))
end, table=function(x)
  local out = {tag="list", n=0}
  local temp, v = next1(x)
  while temp ~= nil do
    local _offset, _result, _temp = 0, {tag="list"}
    _result[1 + _offset] = pretty1(temp) .. " " .. pretty1(v)
    _temp = out
    for _c = 1, _temp.n do _result[1 + _c + _offset] = _temp[_c] end
    _offset = _offset + _temp.n
    _result.n = _offset + 1
    out = _result
    temp, v = next1(x, temp)
  end
  return "{" .. (concat1(out, " ") .. "}")
end, multimethod=function(x)
  return "«method: (" .. getmetatable1(x)["name"] .. " " .. concat1(getmetatable1(x)["args"], " ") .. ")»"
end, ["demand-failure"]=function(failure)
  return demandFailure_2d3e_string1(failure)
end, clojurelist=function(x)
  x["tag"] = "list"
  local lss = pretty1(x)
  return "[" .. sub1(lss, 2, n1(lss) - 1) .. "]"
end, rational=function(x)
  local xn, xd = normalisedRationalComponents1(x)
  return formatOutput_21_1(nil, "" .. format1("%d", xn) .. "/" .. format1("%d", xd))
end}, tag="multimethod", default=function(x)
  if table_3f_1(x) then
    return pretty1["lookup"]["table"](x)
  else
    return tostring1(x)
  end
end}
pretty1 = setmetatable1(this, {__call=function(this1, x)
  local method = this["lookup"][type1(x)] or this["default"]
  if not method then
    error1("No matching method to call for (" .. concat1(list1("pretty", type1(x)), " ") .. ")")
  end
  return method(x)
end, name="pretty", args=list1("x")})
traceback1 = debug and debug.traceback
demandFailure_2d3e_string1 = function(failure)
  if failure["message"] then
    return format1("demand not met: %s (%s).\n%s", failure["condition"], failure["message"], failure["traceback"])
  else
    return format1("demand not met: %s.\n%s", failure["condition"], failure["traceback"])
  end
end
_2a_demandFailureMt_2a_1 = {__tostring=demandFailure_2d3e_string1}
demandFailure1 = function(message, condition)
  return setmetatable1({tag="demand-failure", message=message, traceback=(function()
    if traceback1 then
      return traceback1("", 2)
    else
      return ""
    end
  end)(), condition=condition}, _2a_demandFailureMt_2a_1)
end
abs1 = math.abs
ceil1 = math.ceil
min1 = math.min
modf1 = math.modf
car2 = function(x)
  if not (type1(x) == "list") then
    error1(demandFailure1(nil, "(= (type x) \"list\")"))
  end
  return car1(x)
end
map2 = function(fn, ...)
  local xss = _pack(...) xss.tag = "list"
  local ns
  local out = {tag="list", n=0}
  local forLimit = n1(xss)
  local i = 1
  while i <= forLimit do
    if _enot1(list_3f_1(nth1(xss, i))) then
      error1("that's no list! " .. pretty1(nth1(xss, i)) .. " (it's a " .. type1(nth1(xss, i)) .. "!)")
    end
    push_21_1(out, n1(nth1(xss, i)))
    i = i + 1
  end
  ns = out
  local out = {tag="list", n=0}
  local forLimit = apply1(min1, ns)
  local i = 1
  while i <= forLimit do
    push_21_1(out, apply1(fn, nths1(xss, i)))
    i = i + 1
  end
  return out
end
partition1 = function(p, xs)
  if not (type1(p) == "function") then
    error1(demandFailure1(nil, "(= (type p) \"function\")"))
  end
  if not (type1(xs) == "list") then
    error1(demandFailure1(nil, "(= (type xs) \"list\")"))
  end
  local passed, failed = {tag="list", n=0}, {tag="list", n=0}
  local forLimit = n1(xs)
  local i = 1
  while i <= forLimit do
    local x = nth1(xs, i)
    push_21_1((function()
      if p(x) then
        return passed
      else
        return failed
      end
    end)(), x)
    i = i + 1
  end
  return splice1(list1(passed, failed))
end
filter1 = function(p, xs)
  return first1(partition1(p, xs))
end
nth1 = function(xs, idx)
  if idx >= 0 then
    return xs[idx]
  else
    return xs[xs["n"] + 1 + idx]
  end
end
nths1 = function(xss, idx)
  local out = {tag="list", n=0}
  local forLimit = n1(xss)
  local i = 1
  while i <= forLimit do
    push_21_1(out, nth1(nth1(xss, i), idx))
    i = i + 1
  end
  return out
end
push_21_1 = function(xs, ...)
  local vals = _pack(...) vals.tag = "list"
  if not (type1(xs) == "list") then
    error1(demandFailure1(nil, "(= (type xs) \"list\")"))
  end
  local nxs = n1(xs)
  xs["n"] = (nxs + n1(vals))
  local forLimit = n1(vals)
  local i = 1
  while i <= forLimit do
    xs[nxs + i] = vals[i]
    i = i + 1
  end
  return xs
end
startsWith_3f_1 = function(str, prefix)
  return sub1(str, 1, #prefix) == prefix
end
read1 = io.read
formatOutput_21_1 = function(out, buf)
  if out == nil then
    return buf
  elseif out == true then
    return print1(buf)
  elseif number_3f_1(out) then
    return error1(buf, out)
  elseif function_3f_1(out) then
    return out(buf)
  else
    return out["write"](out, buf)
  end
end
gcd1 = function(x, y)
  local x1, y2 = abs1(x), abs1(y)
  while not (y2 == 0) do
    x1, y2 = y2, x1 % y2
  end
  return x1
end
numerator1 = function(rational)
  return rational["numerator"]
end
denominator1 = function(rational)
  return rational["denominator"]
end
_2d3e_ratComponents1 = function(y)
  local i, f = modf1(y)
  local f_27_ = 10 ^ (n1(tostring1(f)) - 2)
  if 0 == f then
    return splice1(list1(y, 1))
  else
    local n = y * f_27_
    local g = gcd1(n, f_27_)
    return splice1(list1(n / g, (f_27_ / g)))
  end
end
normalisedRationalComponents1 = function(x)
  if number_3f_1(x) then
    return _2d3e_ratComponents1(x)
  else
    return splice1(list1(numerator1(x), (denominator1(x))))
  end
end
repeater1 = function(token, nchars)
  local tn = n1(token)
  local rep_2a_, rem_2a_ = nchars / tn, 0 - (tn - (nchars - 1) % tn)
  return sub1(rep1(token, ceil1(rep_2a_)), 1, (function()
    if 0 == rem_2a_ then
      return nil
    else
      return rem_2a_
    end
  end)())
end
setCursorPos1 = term.setCursorPos
blit1 = term.blit
clear1 = term.clear
set1 = settings.set
get1 = settings.get
save1 = settings.save
n2 = keys.n
r1 = keys.r
u1 = keys.u
y1 = keys.y
b1 = keys.b
d1 = keys.d
f1 = keys.f
l1 = keys.l
enter1 = keys.enter
copy1 = fs.copy
delete1 = fs.delete
isDir1 = fs.isDir
exists1 = fs.exists
pullEvent1 = os.pullEvent
setComputerLabel1 = os.setComputerLabel
getComputerLabel1 = os.getComputerLabel
run1 = os.run
find1 = peripheral.find
blit2 = function(text, foregroundPt, backgroundPt)
  blit1(text, (function()
    if exists_3f_1(foregroundPt) then
      return repeater1(foregroundPt, n1(text))
    else
      return "0"
    end
  end)(), (function()
    if exists_3f_1(backgroundPt) then
      return repeater1(backgroundPt, n1(text))
    else
      return "f"
    end
  end)())
  return print1("")
end
manualFindDrive1 = function()
  print1("Cant find myself. Where am I?")
  print1("Looking for 'enderporter_setup':")
  while true do
    local temp
    local usrIn = read1()
    if isDir1(usrIn) then
      run1("ls", usrIn)
      temp = true
    elseif exists1(usrIn) then
      temp = false
    else
      temp = true
    end
    if temp then
    else
      return nil
    end
  end
end
local drives = map2(function(wr)
  return wr["getMountPath"]()
end, filter1(function(dr)
  return "enderporter_setup" == dr["getDiskLabel"]()
end, list1(find1("drive"))))
if drives then
  if empty_3f_1(drives) then
    programFp1 = manualFindDrive1()
  else
    local candids = filter1(exists1, map2(function(drfp)
      return drfp .. "/pearl_control.lua"
    end, drives))
    if empty_3f_1(candids) then
      programFp1 = error1("Pearl control program removed from disk")
    else
      programFp1 = car2(candids)
    end
  end
else
  programFp1 = manualFindDrive1()
end
clear1()
setCursorPos1(1, 1)
print1(programFp1)
blit2("--= Enderporter Setup =--", "f", "e1453a2")
if nil_3f_1(getComputerLabel1()) or _enot1(startsWith_3f_1(getComputerLabel1(), "enderporter:")) then
  print1("Set the teleporter label:")
  while true do
    local temp
    local usrIn = read1()
    if empty_3f_1(usrIn) then
      temp = true
    else
      setComputerLabel1("enderporter:" .. usrIn)
      temp = false
    end
    if temp then
    else
      break
    end
  end
end
blit2("Specify side to output to:", "f", "8")
blit2("Signal must be inverted (e.g rs torch)", "7", "f")
print1("- Up")
print1("- Down")
print1("- Front")
print1("- Back")
print1("- Left")
print1("- Right")
while true do
  local temp
  local k = second1(pullEvent1("key"))
  if k == u1 then
    set1("emit-side", "top")
    temp = false
  elseif k == d1 then
    set1("emit-side", "bottom")
    temp = false
  elseif k == f1 then
    set1("emit-side", "front")
    temp = false
  elseif k == b1 then
    set1("emit-side", "back")
    temp = false
  elseif k == l1 then
    set1("emit-side", "left")
    temp = false
  elseif k == r1 then
    set1("emit-side", "right")
    temp = false
  else
    temp = true
  end
  if temp then
  else
    break
  end
end
print1(get1("emit-side"))
print1("Copying program over...")
if exists1("/startup.lua") then
  delete1("/startup.lua")
end
copy1(programFp1, "/startup.lua")
print1("Saving setup...")
save1("enderporter.settings")
print1("")
blit2("Broacast rednet on teleport? [Y/n]", "f", "8")
while true do
  local temp
  local k = second1(pullEvent1("key"))
  if k == y1 then
    set1("broadcast-attempt", "true")
    temp = false
  elseif k == n2 then
    set1("broadcast-attempt", "false")
    temp = false
  elseif k == enter1 then
    set1("broadcast-attempt", "true")
    temp = false
  else
    temp = true
  end
  if temp then
  else
    break
  end
end
print1(get1("broadcast-attempt"))
print1("Saving setup...")
return save1("enderporter.settings")
