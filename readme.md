test program built with Urn. not for multiplayer.
https://urn-lang.com

## Features:
- easy to use setup program
- teleport to any enderporter by typing the waypoint name
- forgot waypoint names? poll for every teleporter
- lol who needs draconic evolution

## Setup instructions:
1. Copy `_setup_enderporter.lua` and `pearl_control.lua` to a floppy disk named **enderporter_setup**
2. Insert the floppy disk into a **turtle** with wireless modem on it
3. Run `disk/_setup_enderporter.lua`, follow steps
> This will overwrite the startup program on the turtle
4. Hold control+r
5. Copy `tele.lua` to a pocket computer with wireless modem


## How to set up an enderporter/how it works:

1. bubble column
2. trapdoor on top
3. the configured teleporter turtle, inverted signal coming from it to the trapdoor
4. throw ender pearl down the bubble column

ender pearl is suspended in the bubble column
when rednet packet is received, trapdoor closes on the enderpearl, causing the teleport

![blocks setup](img/setup.png "")

## Usage:
To teleport, enter `tele [teleporter label]` in pocket computer 
[teleporter label] being whatever you entered in the setup program, or
what you read when you mouse over the turtle, minus "enderporter:" prefix
Afterwards, throw another ender pearl down the bubble column

### Restrictions:
- The computer has to be a turtle because regular computers are not activated on world load until right clicked, but turtles are activated
- Note that the signal from the turtle needs to be inverted. This is so the trapdoor remains powered when the world is loaded, otherwise the trapdoor will close for a brief moment.

## Credits

this is a known vanilla minecraft mechanic, but is normally a hassle to wire up due to no wireless redstone.

im giving it to etho however:
https://youtu.be/mgkfMZlkgO4?t=300

previous version before bubble column - ender pearl interaction existed:
https://youtu.be/Ia6y3HuecaQ?t=221
